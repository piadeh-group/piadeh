#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# PIADEH project
# Authors: Kourosh Mirsohi, Khatereh Azizi, Amir Khosravanizadeh, Xavier Corredor, Mohammad
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import math

import numpy as np
from scipy.spatial import distance


class Pedestrian:
    """Class for simulate the people"""

    def __init__(self, position, velocity, target_point):
        self.position = {"x": position[0], "y": position[1]}
        self.velocity = {"x": velocity[0], "y": velocity[1]}
        self.target_point = {"x": target_point[0], "y": target_point[1]}

    def next_position(self, objects, pedestrians, simulation):
        """
        Go to the next position for the next step, defining a new position, velocity
        and target point based on the total forces in the current position.
        """

        ################
        # Friction force
        def unit_vector(a):
            return a / np.linalg.norm(a)

        def friction_force():
            """
            The friction force is the willing of a person to their target.
            If the current velocity of a person is other than the specified
            drift velocity vector towards the person's target, the force
            is such that roughly in the relax time duration the person
            will have the drift velocity towards their target
            """
            Relax_time = 1
            V_d_magnitude = 1  # magnitude of drift velocity
            drift = np.array([self.target_point["x"], self.target_point["y"]]) - np.array(
                [self.position["x"], self.position["y"]])
            velocity = np.array([self.velocity["x"], self.velocity["y"]])
            return ((V_d_magnitude * unit_vector(drift)) - velocity) / Relax_time

        #########################
        # Walls force environment
        def walls_force():
            """
            Using only "y" coordinate of position of each person, the wall exerts a force in "y" direction.
            The force decreases as 1 over distance (1/y)
            """

            if simulation == "corridor":
                L = 20  # width of the corridor
                r0 = 4  # cut off range
                B = 0.5  # strength of the force
                force_y = 0
                if (L - self.position["y"]) < r0:  # for the y = L wall
                    force_y = -B / (L - self.position["y"]) + B / r0  # pushes the person away from the wall
                elif self.position["y"] < r0:  # for the y = 0 wall
                    force_y = B / self.position["y"] - B / r0  # pushes the person away from the wall
                return np.array([0, force_y])

            if simulation == "classroom":
                L = 18  # length of the classroom
                W = 10  # width of the classroom
                r0 = 0.2  # cut off range
                B = 4  # strength of the force
                force_x, force_y = 0, 0
                if (L - self.position["x"]) < r0:  # for the x = L wall
                    force_x = -B / (L - self.position["x"]) + B / r0
                elif self.position["x"] < r0:  # for the x = 0 wall
                    force_x = B / self.position["x"] - B / r0
                if (W - self.position["y"]) < r0:  # for the y = L wall
                    force_y = -B / (W - self.position["y"]) + B / r0
                elif self.position["y"] < r0:  # for the y = 0 wall
                    force_y = B / self.position["y"] - B / r0

                # if the pedestrian is close to the door, he or she won't feel any force from walls and will go out easily.
                if abs(self.position["x"] - self.target_point["x"]) < 3 * r0 and \
                        abs(self.position["y"] - self.target_point["y"]) < 3 * r0:
                    force_x, force = 0, 0
                return np.array([force_x, force_y])

        def interaction_pedestrians():
            """
            Interaction between people. Considering i'th person, if the j'th person is close enough
             (closer than the cut off), and is in him/her view cone (up to a degree), i feels a repulsive force
             with gaussian decay
            """
            k = np.array([0., 0.])
            A = 1  # strength of force
            R = 3  # cut of range
            degree = 80  # view cone in degree unit. The cone has 2*degree angle
            self_speed = np.array([self.velocity["x"], self.velocity["y"]])
            self_position = np.array([self.position["x"], self.position["y"]])
            for pedestrian in pedestrians:
                others_position = np.array([pedestrian.position["x"], pedestrian.position["y"]])
                if (np.linalg.norm(others_position - self_position)) < 0.0000000000001:  # avoid self force
                    continue
                elif np.dot(unit_vector(others_position - self_position), unit_vector(self_speed)) < np.cos(
                        degree * np.pi / 180):
                    continue
                elif np.linalg.norm(others_position - self_position) > 2 * R:  # cut off range
                    continue
                else:
                    k += A * (np.exp(-(np.linalg.norm(others_position - self_position) / R) ** 2) \
                              - np.exp(-4)) * unit_vector(others_position - self_position)
            return k

        def interaction_objects():
            """
            The force of the tables to the pedestrians. The table is made of many points
            which exerts an exp(-r/rcut) to the pedestrianes, if r<rcut
            """
            tables_x = []
            tables_y = []
            for object_n in objects:
                tables_x.append(object_n.position["x"]) #,object_n.position["y"])
                tables_y.append(object_n.position["y"])
            tables = np.zeros((len(tables_x),2))
            for n in range(len(tables_x)):
                tables[n][0] = tables_x[n]
                tables[n][1] = tables_y[n]
            mydist = list(distance.cdist([(self.position["x"], self.position["y"])], tables, 'euclidean'))
            r = min(mydist[0]) 
            
            rcut_tables = 0.2
            K = np.array([0., 0.])
            A = 0.5   # strength of force
            dx = object_n.position["x"] - self.position["x"]
            dy = object_n.position["y"] - self.position["y"]

            if r < rcut_tables:
                force_r = A*math.exp(-r/rcut_tables) - A*math.exp(-1)
                K[0] = force_r*dx/r
                K[1] = force_r*dy/r
            return K

        #############
        # Total force
        total_force = friction_force() + walls_force() - interaction_pedestrians() - interaction_objects()
        # add noise force only for the classroom problem
        if simulation == "classroom":
            total_force += 0.05*np.random.randn() * total_force 

        ###############################
        # euler integation
        def euler(x, xdot, dt):
            """Euler expansion for solving first order
            differential equation: dx/dt = f(x)
            x_{n+1} = euler(x_n, f_n, dt)"""
            xdot_dt = np.multiply(xdot, dt)
            x = np.add(x, xdot_dt)
            return x

        ################################
        # update position and velocity
        if simulation == "corridor":
            dt = 0.4
        if simulation == "classroom":
            dt = 0.2
        self.position["x"] = euler(self.position["x"], self.velocity["x"], dt)
        self.position["y"] = euler(self.position["y"], self.velocity["y"], dt)

        self.velocity["x"] = euler(self.velocity["x"], total_force[0], dt)
        self.velocity["y"] = euler(self.velocity["y"], total_force[1], dt)

        # only for the corridor problem, update the target position in y axe
        if simulation == "corridor":
            self.target_point["y"] = self.position["y"]
        if simulation == "classroom":
            if self.position["x"]>=16:
                self.target_point["x"]=17.5
                self.target_point["y"]=10
            elif self.position["y"]<2.8:
                self.target_point["x"]=self.position["x"]
                self.target_point["y"]=3
            elif (self.position["y"]>=2.8) & (self.position["y"]<3.2):
                self.target_point["x"]=16
                self.target_point["y"]=3
            elif (self.position["y"]>=3.2) & (self.position["y"]<4):
                self.target_point["x"]=self.position["x"]
                self.target_point["y"]=3
            elif (self.position["y"]>=4) & (self.position["y"]<6.8):
                self.target_point["x"]=self.position["x"]
                self.target_point["y"]=7
            elif (self.position["y"]>=6.8) & (self.position["y"]<7.2):
                self.target_point["x"]=16
                self.target_point["y"]=7
            elif self.position["y"]>=7.2:
                self.target_point["x"]=self.position["x"]
                self.target_point["y"]=7
            else:
                self.target_point["x"]=16
                self.target_point["y"]=7


