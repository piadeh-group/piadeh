#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# PIADEH project
# Authors: Kourosh Mirsohi, Khatereh Azizi, Amir Khosravanizadeh, Xavier Corredor, Mohammad
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from random import uniform
import numpy as np

from environment import Corridor, Classroom
from object import Object
from pedestrian import Pedestrian


def corridor_simulation():
    """
    Environment 1: two crowds walking in a corridor in opposite directions
    """

    # define the environment
    walls = [100, 20]
    num_pedestrians = 80

    # create the objects
    objects = []
    for idx in range(10):
        objects.append(Object([uniform(0, 100), uniform(0, 20)], "point", ""))

    # create the pedestrians
    pedestrians = []
    # left pedestrians
    for idx in range(int(num_pedestrians/2)):
        position = [uniform(0, walls[0]*0.1), uniform(0, walls[1])]
        velocity = [0, 0]
        target_point = [walls[0], position[1]]
        pedestrians.append(Pedestrian(position, velocity, target_point))
    # right pedestrians
    for idx in range(int(num_pedestrians/2)):
        position = [walls[0] - uniform(0, walls[0]*0.1), uniform(0, walls[1])]
        velocity = [0, 0]
        target_point = [0, position[1]]
        pedestrians.append(Pedestrian(position, velocity, target_point))

    corridor = Corridor(objects, pedestrians, walls)
    corridor.do_animate()


def classroom_simulation():
    """
    Environment 2: Simulate the evacuation of a classroom
    """

    # define the environment
    walls = [18, 10]
    num_pedestrians = 43
    
    #### mesh=0 (free space), 1 (pedestrane), 2 (table)
    mesh = np.zeros((18, 10), dtype=np.int8)
    mesh[0, 0] = 1
    mesh[0, 1] = 1
    for i in range(1, 14, 2):
        mesh[i, 0] = 2
        mesh[i, 4] = 1
        mesh[i, 5] = 1
        mesh[i, 8] = 1
        mesh[i, 9] = 1
    for i in range(2, 13, 2):
        mesh[i, 0] = 1
        mesh[i, 1] = 1
        mesh[i, 4] = 2
        mesh[i, 8] = 2
    # mesh[14,1] = 2
    mesh[14, 4] = 2
    mesh[14, 8] = 2
    # mesh[15,0] = 1
    # mesh[15,1] = 2 top table

    # create the objects
    objects = []
    id1, id2 = np.where(mesh == 2)

    npoints_in_1m = 50
    binsize = 1/npoints_in_1m

    for index in range(len(id1)):
        for n in range(npoints_in_1m):
            position = [id1[index] + n * binsize, id2[index]]          # left side of table
            objects.append(Object(position, "point", ""))
            position = [id1[index] + (n+1) * binsize, id2[index] + 2]  # right side of table
            objects.append(Object(position, "point", ""))
    
        for n in range(npoints_in_1m*2):
            position = [id1[index], id2[index] + (n+1) * binsize]      # bottom side of table
            objects.append(Object(position, "point", ""))
            position = [id1[index] + 1, id2[index] + n * binsize]      # top side of table
            objects.append(Object(position, "point", ""))
    
    for n in range(npoints_in_1m*2):
        position = [14 + (n + 1) * binsize, 1]  # left side of teacher table 
        objects.append(Object(position, "point", ""))
        position = [14 + (n + 1) * binsize, 2]  # right side of teacher table
        objects.append(Object(position, "point", ""))
    
    for n in range(npoints_in_1m - 1):
        position = [16, 1 + (n + 1) * binsize]  # top side of teacher table
        objects.append(Object(position, "point", ""))

    # create the pedestrians
    pedestrians = []
    id1, id2 = np.where(mesh == 1)
    velocity = [0, 0]
    target_point = [16, 10]

    for index in range(len(id1)):
        position = [id1[index]+0.5, id2[index]+0.5]
        pedestrians.append(Pedestrian(position, velocity, target_point))
    pedestrians.append(Pedestrian([15.0,0.5], velocity, target_point))  # teacher

    classroom = Classroom(objects, pedestrians, walls)
    classroom.do_animate()


#corridor_simulation()
classroom_simulation()
