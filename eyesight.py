import math

def dotproduct(r, xdot):
  return sum((a*b) for a, b in zip(r, xdot))

def length(v):
  return math.sqrt(dotproduct(v, v))

def cos(r, xdot):
  return (dotproduct(r, xdot) / (length(r) * length(xdot)))

def eysight(r, xdot):
    if cos(r, xdot) > 0.17 and length(r) < rc :  # cos(80) = 0.17, rc: cutoff range.
        return 1  # our person sees the other one
    else:
        return 0  # our person dosen't see the other one
