
import numpy as np


def friction():
    pos=np.array([1,2])
    target_point=np.array([3,3])
    vel=np.array([1,-1])
    print(friction(pos,vel,target_point))
    print(np.linalg.norm(friction(pos,vel,target_point)))


def func(x):
    return x + 1


def test_answer():
    assert func(3) == 4

