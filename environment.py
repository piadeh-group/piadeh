#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# PIADEH project
# Authors: Kourosh Mirsohi, Khatereh Azizi, Amir Khosravanizadeh, Xavier Corredor, Mohammad
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import math
from copy import deepcopy

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


class Corridor:
    """Class for corridor simulation

    Environment class for set the objects, pedestrians and target destination for
    simulate two crowds walking in a corridor in opposite directions
    """

    def __init__(self, objects, pedestrians, walls):
        """Initialize the environment, the pedestrians and some plotting function"""

        # set the type of simulation
        self.simulation = "corridor"

        self.objects = objects
        self.pedestrians = pedestrians

        # start visualization
        # ,door_center_x,door_center_y,width
        self.fig, ax = plt.subplots(figsize=(15,3))

        ax.set_xlim(0 - 1, walls[0] + 1)
        ax.set_ylim(0 - 1, walls[1] + 1)

        # door_x=[door_center_x-width)]

        box_x = [0.0, walls[0], walls[0], 0.0, 0.0]
        box_y = [0.0, 0.0, walls[1], walls[1], 0.0]

        self.time_history = []
        self.data = np.zeros((len(self.pedestrians), 2))
        for i in range(30):
            self.time_history.append(self.data.copy())
        print(self.time_history)


        self.clr = []
        for pedestrian in self.pedestrians:
            if pedestrian.target_point["x"] == 0:
                self.clr.append("blue")
            elif pedestrian.target_point["x"] == walls[0]:
                self.clr.append("red")
            else:
                self.clr.append("black")

        self.draw_pedestrians = ax.scatter([], [], s=20, c=self.clr)
        self.draw_pedestrians_trj1 = ax.scatter([], [], s=2, c='.5')
        self.draw_pedestrians_trj2 = ax.scatter([], [], s=2, c='.5')
        self.draw_pedestrians_trj3 = ax.scatter([], [], s=2, c='.5')
        self.draw_pedestrians_trj4 = ax.scatter([], [], s=2, c='.5')
        self.box = ax.plot([], [], '-', ms=20 , color='green')[0]
        self.box.set_data(box_x, box_y)

    def animate(self, i):
      #  print(i)

        pedestrians = deepcopy(self.pedestrians)
        # update the pedestrians, go to the next step
        for p in pedestrians:
            p.next_position(self.objects, pedestrians, self.simulation)
        self.pedestrians = pedestrians

        xs = [p.position["x"] for p in self.pedestrians]
        ys = [p.position["y"] for p in self.pedestrians]

        self.data[:, 0] = xs[:]
        self.data[:, 1] = ys[:]
        
        self.time_history[:-1]=self.time_history[1:]
        self.time_history[-1] = self.data.copy()

        self.draw_pedestrians.set_offsets(self.data)  # update the data
        self.draw_pedestrians_trj1.set_offsets(self.time_history[26])
        self.draw_pedestrians_trj2.set_offsets(self.time_history[23])
        self.draw_pedestrians_trj3.set_offsets(self.time_history[20])
        self.draw_pedestrians_trj4.set_offsets(self.time_history[17])
        return self.draw_pedestrians, self.box, self.draw_pedestrians_trj1, self.draw_pedestrians_trj2, self.draw_pedestrians_trj3, self.draw_pedestrians_trj4

    def do_animate(self):

        ani = animation.FuncAnimation(self.fig, self.animate, interval=5, blit=True)

        # ani.save('basic_animation.mp4', fps=30,
        #          extra_args=['-vcodec', 'libx264'])
        # or
        plt.show()


class Classroom:
    """Class for classroom simulation

    Environment class for set the objects, pedestrians and target destination for
    simulate two crowds walking in a corridor in opposite directions
    """

    def __init__(self, objects, pedestrians, walls):
        """Initialize the environment, the pedestrians and some plotting function"""

        # set the type of simulation
        self.simulation = "classroom"

        self.objects = objects
        self.pedestrians = pedestrians

        # start visualization
        # ,door_center_x,door_center_y,width
        self.fig, ax = plt.subplots(figsize=(14,8))

        ax.set_xlim(0 - 1, walls[0] + 1)
        ax.set_ylim(0 - 1, walls[1] + 1)

        # door_x=[door_center_x-width)]

        box_x = [0.0, walls[0], walls[0], 0.0, 0.0]
        box_y = [0.0, 0.0, walls[1], walls[1], 0.0]

        self.time_history = []
        self.data = np.zeros((len(self.pedestrians), 2))
        for i in range(30):
            self.time_history.append(self.data.copy())
        print(self.time_history)


        # tables
        xt = [o.position["x"] for o in self.objects]
        yt = [o.position["y"] for o in self.objects]

       # self.clr = []
        #for pedestrian in self.pedestrians:
         #   self.clr.append("red")

        self.draw_pedestrians = ax.scatter([], [], s=30, c='red')
        self.box = ax.plot([], [], '-', ms=20, color='green')[0]
        self.draw_pedestrians_trj1 = ax.scatter([], [], s=15, c='pink')
        self.draw_pedestrians_trj2 = ax.scatter([], [], s=15, c='pink')
        self.draw_pedestrians_trj3 = ax.scatter([], [], s=15, c='pink')
        self.draw_pedestrians_trj4 = ax.scatter([], [], s=15, c='pink')
        self.table = ax.plot(xt, yt, 's', ms=5, color='black')[0]
        self.box.set_data(box_x, box_y)

    def animate(self, i):
      #  print(i)

        pedestrians = deepcopy(self.pedestrians)
        # update the pedestrians, go to the next step
        for p in pedestrians:
            p.next_position(self.objects, pedestrians, self.simulation)
        self.pedestrians = pedestrians

        xs = [p.position["x"] for p in self.pedestrians]
        ys = [p.position["y"] for p in self.pedestrians]

        self.data[:, 0] = xs[:]
        self.data[:, 1] = ys[:]
        
        self.time_history[:-1]=self.time_history[1:]
        self.time_history[-1] = self.data.copy()


        self.draw_pedestrians.set_offsets(self.data)  # update the data
        self.draw_pedestrians_trj1.set_offsets(self.time_history[26])
        self.draw_pedestrians_trj2.set_offsets(self.time_history[23])
        self.draw_pedestrians_trj3.set_offsets(self.time_history[20])
        self.draw_pedestrians_trj4.set_offsets(self.time_history[17])
        return self.draw_pedestrians, self.box, self.draw_pedestrians_trj1, self.draw_pedestrians_trj2, self.draw_pedestrians_trj3, self.draw_pedestrians_trj4,self.table


    def do_animate(self):

        ani = animation.FuncAnimation(self.fig, self.animate, interval=10, blit=True)

        # ani.save('basic_animation.mp4', fps=30,
        #          extra_args=['-vcodec', 'libx264'])
        # or
        plt.show()
