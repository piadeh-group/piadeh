#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# PIADEH project
# Authors: Kourosh Mirsohi, Khatereh Azizi, Amir Khosravanizadeh, Xavier Corredor, Mohammad
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


class Object:
    """
    Objects inside the environment
    """

    def __init__(self, position, type, shape):
        self.position = {"x": position[0], "y": position[1]}
        # "point" or "line"
        self.type = type
        # TODO: define how manage the shape
        self.shape = shape